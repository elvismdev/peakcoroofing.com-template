<?php
/* Custom functions code goes here. */


// Removes thematic_blogtitle from the thematic_header phase
function remove_default_contact_form_actions() {
	remove_action('wp_ajax_sendContact','us_sendContact');
	remove_action('wp_ajax_nopriv_sendContact','us_sendContact');
}
// Call 'remove_thematic_actions' during WP initialization
add_action('after_setup_theme','remove_default_contact_form_actions');

// Add our custom function to the 'thematic_header' phase
// add_action('thematic_header','fancy_theme_blogtitle', 3);

function us_sendContact()
{
	global $smof_data, $infusionsoft;
	$errors = 0;

	if (@$smof_data['contact_form_mailchimp'] == 1 AND @$smof_data['contact_form_mailchimp_api_key'] != '' AND @$smof_data['contact_form_mailchimp_list_id'] != '')
	{
		if (empty($_POST['name']))
		{
			$errors++;
		}

		if (empty($_POST['email']))
		{
			$errors++;
		}

		if ($errors > 0)
		{
			$response = array ('success' => 0);
			echo json_encode($response);
			die();
		}

		require_once(get_template_directory().'/vendor/MCAPI.class.php');
		$api = new MCAPI($smof_data['contact_form_mailchimp_api_key']);

		if ($api->listSubscribe( $smof_data['contact_form_mailchimp_list_id'], $_POST['email'], array('FNAME' => $_POST['name'])) === TRUE)
		{
                // It worked!
			$response = array ('success' => 1);
			echo json_encode($response);
			die();
		}
		elseif ($api->errorCode == 214)
		{
                // Already signed up
			$response = array ('success' => 1);
			echo json_encode($response);
			die();
		}
		else
		{
                // An error ocurred, return error message
			$response = array ('success' => 0, error => $api->errorMessage);
			echo json_encode($response);
			die();
		}
	}
	else
	{
		if (in_array(@$smof_data['contact_form_name_field'], array('Shown, required')) AND empty($_POST['name']))
		{
			$errors++;
		}

		if (in_array(@$smof_data['contact_form_email_field'], array('Shown, required')) AND empty($_POST['email']))
		{
			$errors++;
		}

		if (in_array(@$smof_data['contact_form_phone_field'], array('Shown, required')) AND empty($_POST['phone']))
		{
			$errors++;
		}

		if (in_array(@$smof_data['contact_form_message_field'], array('Shown, required')) AND empty($_POST['message']))
		{
			$errors++;
		}

		if ($errors > 0)
		{
			$response = array ('success' => 0);
			echo json_encode($response);
			die();
		}

		$emailTo = (@$smof_data['contact_form_email'] != '')?$smof_data['contact_form_email']:get_option('admin_email');

		$body = '';

		if (in_array(@$smof_data['contact_form_name_field'], array('Shown, required', 'Shown, not required')))
		{
			$body .= __('Name', 'us').": ".$_POST['name']."\n";
		}

		if (in_array(@$smof_data['contact_form_email_field'], array('Shown, required', 'Shown, not required')))
		{
			$body .= __('Email', 'us').": ".$_POST['email']."\n";
		}

		if (in_array(@$smof_data['contact_form_phone_field'], array('Shown, required', 'Shown, not required')))
		{
			$body .= __('Phone', 'us').": ".$_POST['phone']."\n";
		}

		if (in_array(@$smof_data['contact_form_message_field'], array('Shown, required', 'Shown, not required')))
		{
			$body .= __('Message', 'us').": ".$_POST['message']."\n";
		}

		// Add a contact using the ContactService.add method
		$first_and_last_name = explode( ' ', $_POST['name'], 2 );

		$contact = $infusionsoft->contact( 'addWithDupCheck', array(
			'Email' => $_POST['email'],
			'FirstName' => ( ( isset( $first_and_last_name[0] ) ) ? $first_and_last_name[0] : '' ),
			'LastName' => ( ( isset( $first_and_last_name[1] ) ) ? $first_and_last_name[1] : '' ),
			'Phone1' => $_POST['phone']
			), 'Email' );

		// Check whether or not the API returned an error
		if ( ! is_wp_error( $contact ) ) {
    	// The $contact variable contains the return value as specified in the documentation
			// Add a contact tag using the ContactService.addToGroup method
			$is_tag = $infusionsoft->contact( 'addToGroup', $contact, 288); // Adding to New England Lead tag.

			// Check whether or not the API returned an error
			if ( is_wp_error( $is_tag ) )
				error_log( 'There was an error! Message: ' . $is_tag->get_error_message() );

		} else {
			error_log( 'There was an error! Message: ' . $contact->get_error_message() );
		}

		$headers = '';

		wp_mail($emailTo, __('Contact request from', 'us')." http://".$_SERVER['HTTP_HOST'].'/', $body, $headers);

		$response = array ('success' => 1);
		echo json_encode($response);
	}



	die();

}

add_action( 'wp_ajax_nopriv_sendContact', 'us_sendContact', 3 );
add_action( 'wp_ajax_sendContact', 'us_sendContact', 3 );